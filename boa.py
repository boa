# Scheme in Python
#
# This is my first attempt to write LISP in Python. The code is
# based on the metacircular evaluator explored in ch4, SICP. I have
# kept it as pedagogic as possible. For this reason it is not
# pragmatic. There is a partial macro implementation (define-syntax
# without the ellipsis). Additionally it is also possible to access Python
# modules/objects from Scheme and vice versa (look at the module import
# hooks).

# Honestly, I am planning to write a `real' LISP in Python,
# keeping in mind the best of both worlds: syntax/macros in Scheme;
# libraries in Python.

# See tests.py for test cases

#  - Basic Scheme
#  - Access Python in Scheme
#  - Access Scheme in Python
#  - pedagogic source code
#  - An inconceivably inefficient evaluator

__author__ = 'Sridhar Ratna'

import sys, os, ihooks

## lisp emulation functions
###########################
    
def error(s):
    raise s

class tagged(object):
    def __init__(self, tag, value):
        self.tag = tag
        self.value = value
    def __eq__(self, other):
        return type(other) is tagged and \
            self.tag == other.tag and self.value == other.value
    def __repr__(self):
        return "<TAG:%s %s >" % (self.tag, self.value)

def symbol(string): return tagged("symbol", string)
def symbol_p(o):    return type(o) is tagged and o.tag == "symbol"
def symbol_to_string(o): return o.value

def cons(x, y): return [x, y]
def car(pair):  return pair[0]
def cdr(pair):  return pair[1]
def cadr(x): return car(cdr(x))
def caar(x): return car(car(x))
def cddr(x): return cdr(cdr(x))
def caddr(x): return car(cdr(cdr(x)))
def caadr(x): return car(car(cdr(x)))
def cadar(x): return car(cdr(car(x)))
def cdadr(x): return cdr(car(cdr(x)))
def caddr(x): return car(cdr(cdr(x)))
def cadddr(x): return car(cdr(cdr(cdr(x))))

def pair_p(o):   return type(o) is list and len(o)==2

def set_caR(pair, value):
    assert pair_p(pair)
    pair[0] = value
    return symbol("ok")

def set_cdR(pair, value):
    assert pair_p(pair)
    pair[1] = value
    return symbol("ok")

def slist(*args):
    if len(args) == 0:
        return None
    else:
        return cons(args[0], slist(*args[1:]))

def smap(func, sl):
    if null_p(sl):
        return null
    else:
        return cons(func(car(sl)),
                    smap(func, cdr(sl)))

def slength(sl):
    if null_p(sl):
        return 0
    else:
        return 1+slength(cdr(sl))

def smember(sl, e):
    if null_p(sl):
        return False
    elif eq_p(car(sl), e):
        return True
    else:
        return smember(cdr(sl), e)
    
null = slist()
def null_p(o): return o is None # ?? coerce

def eq_p(o1, o2):
    # ??
    return o1 == o2 # ??

def number_p(o):
    return type(o) in (int, long, float)

def string_p(o):
    return type(o) is str

# coercion (TODO)

def slist2pylist(slst):
    if null_p(slst):
        return []
    else:
        return [car(slst)] + slist2pylist(cdr(slst))

def dot(o, *args):
    "a.b.c in Python is translated to (dot a b c) in Lisp"
    for arg in args:
        o = getattr(o, arg)
    return o

def l2py(o):
    if null_p(o):
        # there is no equivalent of None in Lisp as of now
        # '() is considered nothing more than an empty list
        return list()
    elif pair_p(o):
        # there is no way we can coerce a `pair' only to
        # get it back as it was
        # we also make a deep copy using `l2py' itself
        return map(l2py, slist2pylist(o))
    else:
        return o

def py2l(o):
    if type(o) in (list, tuple):
        return slist(*o)
    else:
        return o

py_apply = apply

## eval/appy
############

def eval(exp, env):
    if is_self_evaluating(exp):
        return exp
    if is_variable(exp):
        return lookup_variable_value(exp, env)
    if is_quoted(exp):
        return text_of_quotation(exp)
    if is_assignment(exp):
        return eval_assignment(exp, env)
    if is_definition(exp):
        return eval_definition(exp, env)
    if is_syntax_definition(exp):
        return eval_syntax_definition(exp, env)
    if is_if(exp):
        return eval_if(exp, env)
    if is_lambda(exp):
        return make_procedure(lambda_parameters(exp),
                              lambda_body(exp),
                              env)
    if is_begin(exp):
        return eval_sequence(begin_actions(exp), env)
    if is_cond(exp):
        return eval(cond_to_if(exp), env)
    if is_application(exp):
        op = eval(operator(exp), env)
        if syntax_rules_p(op):
            return eval(transform_syntax(op, operands(exp)), env)
        else:
            return apply(op, list_of_values(operands(exp), env))
    # else
    error("Unknown expression type -- EVAL: %s" % exp)

def apply(procedure, arguments):
    if is_primitive_procedure(procedure):
        return apply_primitive_procedure(procedure, arguments)
    if is_compound_procedure(procedure):
        return eval_sequence(
            procedure_body(procedure),
            extend_environment(procedure_parameters(procedure),
                               arguments,
                               procedure_environment(procedure)))
    if is_python_callable(procedure):
        return apply_python_procedure(procedure, arguments)
    # else
    error("Unknown procedure type -- APPLY: %s" % procedure)

def list_of_values(exps, env):
    if no_operands(exps):
        return null
    else:
        return cons(eval(first_operand(exps), env),
                    list_of_values(rest_operands(exps), env))

def eval_if(exp, env):
    if is_true(eval(if_predicate(exp), env)):
        return eval(if_consequent(exp), env)
    else:
        return eval(if_alternative(exp), env)

def eval_sequence(exps, env):
    if is_last_exp(exps):
        return eval(first_exp(exps), env)
    else:
        eval(first_exp(exps), env)
        return eval_sequence(rest_exps(exps), env)

def eval_assignment(exp, env):
    set_variable_valuE(assignment_variable(exp),
                       eval(assignment_value(exp), env),
                       env)
    return symbol("ok")

def eval_definition(exp, env):
    define_variablE(definition_variable(exp),
                    eval(definition_value(exp), env),
                    env)
    return symbol("ok")

def is_self_evaluating(exp):
    return number_p(exp) or string_p(exp)

def is_variable(exp):
    return symbol_p(exp)

def is_quoted(exp):
    return is_tagged_list(exp, symbol("quote"))

def text_of_quotation(exp):
    return cadr(exp)

def is_tagged_list(exp, tag):
    return pair_p(exp) and eq_p(car(exp), tag)

def is_assignment(exp):
    return is_tagged_list(exp, symbol("set!"))

def assignment_variable(exp):
    return cadr(exp)

def assignment_value(exp):
    return caddr(exp)

def is_definition(exp):
    return is_tagged_list(exp, symbol("define"))

def definition_variable(exp):
    if symbol_p(cadr(exp)):
        return cadr(exp)
    else:
        return caadr(exp)

def definition_value(exp):
    if symbol_p(cadr(exp)):
        return caddr(exp)
    else:
        return make_lambda(cdadr(exp),
                           cddr(exp))

def is_lambda(exp):
    return is_tagged_list(exp, symbol("lambda"))

def lambda_parameters(exp):
    return cadr(exp)

def lambda_body(exp):
    return cddr(exp)

def make_lambda(parameters, body):
    return cons(symbol("lambda"),
                cons(parameters, body))

def is_if(exp):
    return is_tagged_list(exp, symbol("if"))

def if_predicate(exp):
    return cadr(exp)

def if_consequent(exp):
    return caddr(exp)

def if_alternative(exp):
    if not null_p(cdddr(exp)):
        return cadddr(exp)
    else:
        return symbol("FALSE")

def make_if(predicate, consequent, alternative):
    return slist(symbol("IF"),
                 predicate, consequent, alternative)

def is_begin(exp):
    return is_tagged_list(exp, symbol("begin"))

def begin_actions(exp):
    return cdr(exp)

def is_last_exp(seq):
    return null_p(cdr(seq))

def first_exp(seq):
    return car(seq)

def rest_exps(seq):
    return cdr(seq)

def sequence_to_exp(seq):
    if null_p(seq):
        return seq
    elif is_last_exp(seq):
        return first_exp(seq)
    else:
        return make_begin(seq)

def make_begin(seq):
    return cons(symbol("begin"),
                seq)

def is_application(exp):
    return pair_p(exp)

def operator(exp):
    return car(exp)

def operands(exp):
    return cdr(exp)

def no_operands(ops):
    return null_p(ops)

def first_operand(ops):
    return car(ops)

def rest_operands(ops):
    return cdr(ops)

# The following are `derived' expressions (apt to be macros)

def is_cond(exp):
    return is_tagged_list(exp, symbol("cond"))

def cond_clauses(exp):
    return cdr(exp)

def is_cond_else_clause(clause):
    return eq_p(cond_predicate(clause),
                symbol("ELSE"))

def cond_predicate(clause):
    return car(clause)

def cond_actions(clause):
    return cdr(clause)

def cond_to_if(exp):
    return expand_clauses(cond_clauses(exp))

def expand_clauses(clauses):
    if null_p(clauses):
        return symbol("FALSE")
    else:
        first = car(clauses)
        rest  = cdr(clauses)
        if is_cond_else_clause(first):
            if null_p(rest):
                sequence_to_exp(cond_actions(first))
            else:
                error("ELSE clause isn't last -- COND->IF: %s'" % clauses)
        else:
            return make_if(cond_predicate(first),
                           sequence_to_exp(cond_actions(first)),
                           expand_clauses(rest))

## evaluator data structures
############################

def is_true(x):
    return l2py(x) is not False # ??

def is_false(x):
    return l2py(x) is False

def make_procedure(parameters, body, env):
    return slist(symbol("procedure"),
                 parameters, body, env)

def is_compound_procedure(p):
    return is_tagged_list(p, symbol("procedure"))

def procedure_parameters(p):
    return cadr(p)

def procedure_body(p):
    return caddr(p)

def procedure_environment(p):
    return cadddr(p)

def enclosing_environment(env):
    return cdr(env)

def first_frame(env):
    return car(env)

the_empty_environment = slist()

def make_frame(variables, values):
    return cons(variables, values)

def frame_variables(frame):
    return car(frame)

def frame_values(frame):
    return cdr(frame)

def add_binding_to_framE(var, val, frame):
    set_caR(frame, cons(var, car(frame)))
    set_cdR(frame, cons(val, cdr(frame)))

def extend_environment(vars, vals, base_env):
    if slength(vars) == slength(vals):
        return cons(make_frame(vars, vals), base_env)
    else:
        error("Too few/many arguments supplied: %s, %s" % (vars, vals))

# debug
def print_environment(env, depth=1):
    print '%sEE %s' % (' '*depth, first_frame(env))
    if not null_p(enclosing_environment(env)):
        print_environment(enclosing_environment(env), depth+2)

def do_variable(var, env, match_thunk, next_thunk=None):
    def env_loop(env):
        def scan(vars, vals):
            if null_p(vars):
                if next_thunk:
                    return next_thunk()
                else:
                    return env_loop(enclosing_environment(env))
            elif eq_p(var, car(vars)):
                return match_thunk(vars, vals)
            else:
                return scan(cdr(vars), cdr(vals))
        if eq_p(env, the_empty_environment):
            raise NameError, "Unbound LISP variable: %s" % var
        else:
            frame = first_frame(env)
            return scan(frame_variables(frame), frame_values(frame))
    return env_loop(env)

def lookup_variable_value(var, env):
    assert symbol_p(var)
    return do_variable(var, env,
                       lambda vars, vals: car(vals))

def set_variable_valuE(var, val, env):
    assert symbol_p(var)
    return do_variable(var, env,
                       lambda vars, vals: set_caR(vals, val))

def define_variablE(var, val, env):
    assert symbol_p(var)
    return do_variable(var, env,
                       lambda vars, vals: set_caR(vals, val),
                       lambda : add_binding_to_framE(var, val,
                                                     first_frame(env)))
    
## parser
#########

import re
c = re.compile

PATTERNS = [
    ('whitespace',     c(r'(\s+)')),
    ('comment',        c(r'(;[^\n]*)')),
    ('(',              c(r'(\()')),
    (')',              c(r'(\))')),
    ('number',         c(r'''( [+\-]?    ## optional sign,
                               (?:       ## followed by some
                                         ## decimals
                                    \d+\.\d+
                                  | \d+\.
                                  | \.\d+
                                  | \d+
                                )
                              )
                                ''',
                         re.VERBOSE)),
    ('symbol',         c(r'''([a-zA-Z\+\=\?\!\@\#\$\%\^\&\*\-\/\.\>\<]
                              [\w\+\=\?\!\@\#\$\%\^\&\*\-\/\.\>\<]*)''',
                         re.VERBOSE)),
    ('string',         c(r'''
                           "
                           (([^\"] | \\")*)
                           "
                           ''',
                         re.VERBOSE)),
    ("'",              c(r'(\')')),
    ('`',              c(r'(`)')),
    (',',              c(r'(,)')),
    ]

def tokenize(s):
    tokens = []
    while s:
        m = None
        for type, regex in PATTERNS:
            m = regex.match(s)
            if m:
                token = m.group(1)
                tokens.append((type, token))
                s = s[m.span()[1]:]
                break
        if not m:
            error("TOKENIZE error from: %s..." % s[:20])
    return tokens

def filter_executable_tokens(tokens):
    return filter(
        lambda x: x[0] not in ('whitespace', 'comment'),
        tokens)

def parse(text):
    tokens = filter_executable_tokens(tokenize(text))
    def parse_till_end(n):
        if n == len(tokens):
            return null
        else:
            sexp, n = parse_sexp(tokens, n)
            return cons(sexp, parse_till_end(n))
    return sequence_to_exp(parse_till_end(0))

def parse_sexp(tokens, fr):
    if tokens[fr][0] is 'string':
        return tokens[fr][1], fr+1
    if tokens[fr][0] is 'number':
        return int(tokens[fr][1]), fr+1 # ??
    if tokens[fr][0] is 'symbol':
        return symbol(tokens[fr][1]), fr+1
    if tokens[fr][0] is "'":
        e, fr = parse_sexp(tokens, fr+1)
        return slist(symbol("quote"), e), fr
    if tokens[fr][0] == '(':
        return parse_rest_of_sexps(tokens, fr+1)
    error("PARSE error -- Invalid/unsupported token: %s" % tokens[fr][0])
        
def parse_rest_of_sexps(tokens, fr):
    if tokens[fr][0] == ')':
        return null, fr+1
    else:
        e, fr = parse_sexp(tokens, fr)
        r, fr = parse_rest_of_sexps(tokens, fr)
        return cons(e, r), fr

## macros (partial)
#########

# Note that these are `first-class' macros. And so the 
# `environment' need not distinguish between variables
# and keywords (macro names). `eval' looks up the operator
# object and determines if it is a procedure application or
# a macro expansion (see syntax_rules_p)
# Ideally, environment table must distinguish between variables
# and keywords. This helps in several ways.
#  - syntax-case can be implemented (and syntax-rules in in terms of it)
#  - No need to `eval' operator to transform the syntax
# 

def syntax_rules(l, p):   return tagged("syntax-rules", cons(l, p))
def syntax_rules_p(o): return type(o) is tagged and o.tag == "syntax-rules"
def literals(sr):
    return car(sr)
def first_pattern(sr):
    return cdr(caar(cdr(sr)))
def first_clause(sr):
    return cadar(cdr(sr))
    
def is_syntax_definition(exp):
    return is_tagged_list(exp, symbol("define-syntax"))

def eval_syntax_definition(exp, env):
    define_variablE(cadr(exp),
                    eval_syntax_transfomer(caddr(exp)),
                    env)

def eval_syntax_transfomer(exp):
    if not eq_p(car(exp), symbol("syntax-rules")):
        error("only SYNTAX-RULES is supported: %s" % exp)
    return syntax_rules(cadr(exp), cddr(exp))

def binding_get(binding, symbol):
    return binding[symbol_to_string(symbol)]
def binding_put(binding, symbol, value):
    binding[symbol_to_string(symbol)] = value
def binding_in(binding, symbol):
    return symbol_to_string(symbol) in binding

def transform_syntax(syntax_rules, operands):
    def transform_loop(rules):
        if null_p(rules):
            error("No PATTERN matched -- syntax-rules: %s" % operands)
        else:
            binding = {}
            if match(first_pattern(rules), literals(rules), operands, binding):
                return expand(first_clause(rules), binding)
            else:
                return transform_loop(cdr(rules))
    return transform_loop(syntax_rules.value)

def match(pattern, literals, operands, binding):
    if null_p(pattern):
        return null_p(operands)
    if null_p(operands):
        return False
    if symbol_p(car(pattern)):
        if not smember(literals, car(pattern)):
            binding_put(binding, car(pattern), car(operands))
    elif pair_p(car(pattern)):
        if not match(car(pattern), literals, car(operands), binding):
            return False
    else:
        error("Invalid PATTERN: %s" % car(pattern))
    return match(cdr(pattern), literals, cdr(operands), binding)
    
def expand(clause, binding):
    if pair_p(clause):
        return cons(expand(car(clause), binding),
                    expand(cdr(clause), binding))
    elif symbol_p(clause) and binding_in(binding, clause):
        return binding_get(binding, clause)
    else:
        return clause


## runtime
##########

def setup_environment():
    initial_env = extend_environment(primitive_procedure_names(),
                                     primitive_procedure_objects(),
                                     the_empty_environment)
    define_variablE(symbol("true"), py2l(True), initial_env)
    define_variablE(symbol("false"), py2l(False), initial_env)
    return initial_env

# primitive procedures

def is_primitive_procedure(proc):
    return is_tagged_list(proc, symbol("primitive"))

def primitive_implementation(proc):
    return cadr(proc)

# ??
primitive_procedures = slist(
    slist(symbol("car"), car),
    slist(symbol("cdr"), cdr),
    slist(symbol("cons"), cons),
    slist(symbol("null?"), null_p),
    slist(symbol("list"), slist),
    slist(symbol("+"), lambda *args: sum(args)),
    slist(symbol("."), dot),
    slist(symbol("import"), lambda m: __import__(m)))

def primitive_procedure_names():
    return smap(car, primitive_procedures)

def primitive_procedure_objects():
    return smap(lambda p: slist(symbol("primitive"), cadr(p)),
                primitive_procedures)

def apply_primitive_procedure(proc, args):
    return py_apply(
        primitive_implementation(proc),
        slist2pylist(args))

def apply_python_procedure(p, args):
    return py_apply(p, l2py(args))

def is_python_callable(procedure):
    return callable(procedure)


class LispModule:

    def __init__(self, module_path, parent_env):
        self.env = extend_environment(null, null,
                                      parent_env)
        self.value = eval(
            parse(open(module_path).read()),
            self.env)

    def __nonzero__(self):
        # `if m: ..' looks up m.__nonzero__
        return True

    def __getattr__(self, attr):
        return lookup_variable_value(symbol(attr), self.env)

class LispModuleLoader(ihooks.ModuleLoader):

    def __init__(self, env):
        self.env = env
        ihooks.ModuleLoader.__init__(self)
        print "INIT"

    def find_module_in_dir(self, name, dir, allow_packages=1):
        if dir:
            return None, name, ('', '', "lisp-module")
        else:
            ihooks.ModuleLoader.find_module_in_dir(
                self, name, dir, allow_packages)

    def load_module(self, name, stuff):
        print "LOAD"
        file, filename, info = stuff
        (suff, mode, type) = info
        if type == "lisp-module":
            m = self._import_module(filename)
            m.__file__ = filename
            return m
        else:
            return ihooks.ModuleLoader.load_module(self, name, stuff)

    def _import_module(self, module):
        if module in sys.modules:
            return sys.modules[module]
        else:
            m = LispModule(module, self.env)
            sys.modules[module] = m
            return m


input_prompt  = ";;; M-Eval input: "
output_prompt = ";;; M-Eval value: "

class Interpreter:

    def __init__(self):
        self.the_global_environment = setup_environment()

    def push(self, text):
        return eval(parse(text), self.the_global_environment)

    def driver_loop(self):
        while True:
            print "\n%s" % input_prompt
            input = raw_input() # ??
            output = eval(parse(input), self.the_global_environment)
            print output_prompt
            self.user_print(output)

    def user_print(self, object):
        if is_compound_procedure(object):
            print slist(symbol("COMPOUND-PROCEDURE"),
                        procedure_parameters(object),
                        procedure_body(object),
                        symbol("<procedure-env>"))
            print object
        else:
            # ??
            print object


def install():
    "Install the default interpreter"
    interpreter = Interpreter()
    ihooks.install(ihooks.ModuleImporter(
                LispModuleLoader(interpreter.the_global_environment)))


if __name__ == "__main__":
    Interpreter().driver_loop()
