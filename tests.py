import boa, os
from nose import with_setup

i = None
def setup():
    global i
    i = boa.Interpreter()

@with_setup(setup)
def test_assignment():
    i.push('(define a "foo")')
    v = i.push("(cons a a)")
    a = boa.cons('foo', 'foo')
    assert boa.eq_p(v, a), (v, a)
    
    i.push('(define b 2)')
    v = i.push('b')
    assert boa.eq_p(v, 2), type(v)

@with_setup(setup)
def test_if():
    v = i.push(r"""
(begin
  (define a true)
  (if a 3 4))
""")
    assert boa.eq_p(v, 3), v

@with_setup(setup)
def test_procedures():
    v = i.push(r"""
(begin
  (define (foo x y)
    (+ x
       (+ y x)))
  (foo 4 5))
""")
    assert boa.eq_p(v, 13)

    v = i.push(r"""
(begin
  (define (bar x)
    (+ x
       ((lambda (a) (+ a a)) x)))
  (bar 2))
""")
    assert boa.eq_p(v, 6), v

@with_setup(setup)
def test_closure():
    v = i.push(r"""
(begin
  (define (counter n)
    (define (inc)
      (set! n (+ n 1))
      n)
    inc)
  (define a1 (counter 3))
  (define a2 (counter 5))
  (a1)
  (a2)
  (a1)
  (a1)
  (a2)
  (cons (a1) (a2)))
""")
    assert boa.eq_p(v, boa.cons(7, 8))

@with_setup(setup)
def test_import():
    v = i.push(r"""
  (define os (import "os"))
  ((. os "getlogin"))
""")
    assert boa.eq_p(v, os.getlogin()), v

def test_import_hook():
    boa.install()
    import foo
    assert foo.bar == 3

@with_setup(setup)
def test_macro():
    v = i.push(r"""
(define-syntax when
  (syntax-rules ()
    ((when c e) (if c e))))

(define a true)
(when a (+ 3 4))
""")
    assert boa.eq_p(v, 7), v
