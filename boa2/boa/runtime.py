"""Lisp runtime
"""

import __builtin__, ihooks

from boa import primitives
from boa.environment import Environment, PyEnvironment
from boa.evaluator import eval
from boa.parser import parse
from boa.lispmod import LispModuleLoader

class Interpreter(object):

    def __init__(self):
        # python builtins
        e = PyEnvironment(None, __builtin__.__dict__)
        # lisp primitives
        e = Environment(e, module_exports(primitives))
        self.env = e
        mod = LispModuleLoader(self.env)
        ihooks.install(ihooks.ModuleImporter(mod))
        
    def push(self, text):
        return eval(parse(text), self.env)

        
def module_exports(mod):
    "Return the module dictionary for mod.__all__ alone"
    namespace = {}
    for var in mod.__all__:
        namespace[var] = mod.__dict__[var]
    return namespace
