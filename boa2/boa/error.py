"""Error functions
"""

class LispError(Exception):
    pass

def error(fmt_string, *args):
    raise LispError, fmt_string % args
