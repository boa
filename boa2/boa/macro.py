"""Scheme hygienic macros

syntax-case is implemented, while syntax-rules can simply be defined
in terms of syntax-case:

(define-syntax syntax-rules
  (lambda (x)
    (syntax-case x ()
      ((_ (i ...) ((keyword . pattern) template) ...)
       (syntax (lambda (x)
                 (syntax-case x (i ...)
                   ((dummy . pattern) (syntax template))
                   ...)))))))

Checklist:
 
  - Basic syntax-case
  - ...
  - fender

"""

from boa.primitives import *
from boa.evaluator import eval, is_tagged_list

# evaluator functions
#####################

def is_syntax_definition(exp):
    return is_tagged_list(exp, symbol("define-syntax"))

class keyword(str):
    def __eq__(self, o):
        return type(o) is keyword and str(self) == str(o)
    def __repr__(self):
        return "<KEYWORD: %s>" % self

def keyword_p(sym):
    return type(sym) is keyword

def eval_syntax_definition(exp, env):
    k = keyword(symbol_to_string(cadr(exp)))
    v = eval(caddr(exp), env)
    env.define(k, v)
    return symbol("ok")

# transformer
#############

# This function is called when the macro is to be `expanded'
# after the lambda (x) wrapper.
def eval_syntax_case(stx, literals, rest):
    """
    stx - syntax object representing the form to be processed
    returns a syntax object representing the output form
    """
    print stx
    print literals
    print rest
    print '=' * 6
    # return list(symbol("set!"), symbol("a"), 540)
    
