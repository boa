"""Python import hooks for importing Lisp modules into Python source
"""

import sys, os, ihooks
from boa.environment import Environment
from boa.evaluator import eval
from boa.parser import parse


class LispModule:

    def __init__(self, module_path, parent_env):
        self.env = Environment(parent_env, {})
        print module_path
        self.value = eval(
            parse(open(module_path).read()),
            self.env)

    def __nonzero__(self):
        # `if m: ..' looks up m.__nonzero__
        return True

    def __getattr__(self, attr):
        return self.env.lookup(attr)

class LispModuleLoader(ihooks.ModuleLoader):

    def __init__(self, env):
        self.env = env
        ihooks.ModuleLoader.__init__(self)

    def find_module_in_dir(self, name, dir, allow_packages=1):
        if dir and os.path.exists(os.path.join(dir, name + ".boa")):
            return None, name, ('', '', "lisp-module")
        else:
            return ihooks.ModuleLoader.find_module_in_dir(
                self, name, dir, allow_packages)

    def load_module(self, name, stuff):
        file, filename, info = stuff
        (suff, mode, type) = info
        if type == "lisp-module":
            m = self._import_module(filename+".boa")
            m.__file__ = filename
            return m
        else:
            return ihooks.ModuleLoader.load_module(self, name, stuff)

    def _import_module(self, module):
        if module in sys.modules:
            return sys.modules[module]
        else:
            m = LispModule(module, self.env)
            sys.modules[module] = m
            return m
