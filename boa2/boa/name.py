"""Name mangling between Python and Scheme names

The following conversion rules are observed,

  Python                    Lisp
  ------                    ------
  foo_bar                   foo-bar
  null_p                    null?
  set_car                   set-car!  (set_*)
  foo_to_bar                foo->bar

"""

def py2lisp(name):
    if name.endswith('_p'):
        name = "%s?" % name[:-2]
    if '_to_' in name:
        name = name.replace('_to_', '->')
    name = name.replace('_', '-')
    if name.startswith('set-'):
        name = "%s!" % name
    return name

def lisp2py(name):
    name = name.replace('->', '_to_')
    if name.endswith('?'):
        name = "%s_p" % name[:-1]
    name = name.replace('-', '_')
    for sp in "!*#@$%":
        name = name.replace(sp, '')
    return name

