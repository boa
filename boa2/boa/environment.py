"""Execution environment
"""

from boa.error import *
from boa.name import lisp2py

class Environment(object):

    def __init__(self, next_env, namespace):
        self.next_env = next_env
        self.namespace = namespace

    def lookup(self, variable, orig_variable=None):
        if variable in self.namespace:
            return self.namespace[variable]
        elif self.next_env:
            return self.next_env.lookup(variable)
        else:
            error("Unbound variable: %s", orig_variable or variable)

    def __contains__(self, variable):
        return variable in self.namespace or \
            (self.next_env and variable in self.next_env)

    def set(self, variable, value):
        if variable in self.namespace:
            self.namespace[variable] = value
        elif self.next_env:
            self.next_env.set(variable, value)
        else:
            error("Unbound variable: %s", variable)


    def define(self, variable, value):
        self.namespace[variable] = value

class PyEnvironment(Environment):

    """This is meant to be used with Python namespaces. Scheme symbols
    are converted to Python names using the name mangling module.
    (see boa/name.py)
    """

    def lookup(self, variable):
        mangled_variable = lisp2py(variable)
        return Environment.lookup(self, mangled_variable, variable)
