"""Standard Lisp primitives

  Lisp type        Python representation
  ---------        ---------------------
  pair             class cons
  '()              None
  symbol           class symbol
  
  
"""

from error import *

__all__ = ["cons", "car", "cdr", "null_p", "pair_p", "pylist",
           "cadr", "caar", "cddr", "caddr", "caadr", "cdadr", "cadddr",
           "set_car", "set_cdr", "symbol", "list", "map",
           "true", "false", "eq_p", "number_p", "symbol_p", "string_p",
           "display", "newline", "symbol_to_string",
           "+", "-"]

# pair
######

class cons(object):
    __slots__ = ('car', 'cdr')
    def __init__(self, car, cdr):
        self.car = car
        self.cdr = cdr
    def __eq__(self, o):
        return pair_p(o) and self.car == o.car and self.cdr == o.cdr

def pair_p(p):
    return type(p) is cons

def null_p(o):
    return o is None

def car(p):
    if pair_p(p): return p.car
    else: error("CAR: expects a non-empty <pair>; given %s", p)

def cdr(p):
    if pair_p(p): return p.cdr
    else: error("CDR: expects a non-empty <pair>; given %s", p)

def set_car(p,v):
    if pair_p(p): p.car = v
    else: error("CAR: expects a non-empty <pair>; given %s", p)

def set_cdr(p,v):
    if pair_p(p): p.cdr = v
    else: error("CDR: expects a non-empty <pair>; given %s", p)

# TODO: complete the combinations
def cadr(x): return car(cdr(x))
def caar(x): return car(car(x))
def cddr(x): return cdr(cdr(x))
def caddr(x): return car(cdr(cdr(x)))
def caadr(x): return car(car(cdr(x)))
def cadar(x): return car(cdr(car(x)))
def cdadr(x): return cdr(car(cdr(x)))
def caddr(x): return car(cdr(cdr(x)))
def cadddr(x): return car(cdr(cdr(cdr(x))))

def list(*args):
    l = None
    for a in reversed(args):
        l = cons(a, l)
    return l

def map(function, ls):
    result = []
    while not null_p(ls):
        result.append(function(car(ls)))
        ls = cdr(ls)
    return list(*result)

def pylist(ls):
    result = []
    while not null_p(ls):
        result.append(car(ls))
        ls = cdr(ls)
    return result

# symbol
########

class symbol(object):
    __slot__ = ('string')
    def __init__(self, string):
        self.string = string
    def __hash__(self):
        return hash(self.string)
    def __eq__(self, o):
        return type(o) is symbol and self.string == o.string
    def __str__(self):
        return "<symbol: %s>" % self.string
    __repr__ = __str__

def symbol_p(sym):
    return type(sym) is symbol

def symbol_to_string(sym):
    if symbol_p(sym): return sym.string
    else: error("symbol->string: expets a <symbol>; given %s", sym)

def string_to_symbol(s):
    return symbol(s)

# misc
######

true = True
false = False

def eq_p(x, y):
    return x == y

def number_p(n):
    return type(n) in (int, float)

def string_p(s):
    return type(s) is str

def display(s):
    print s
    return symbol("ok")

def newline():
    print
    return symbol("ok")

# math
######

globals()["+"] = lambda *args: sum(args)
globals()["-"] = lambda a,b: a-b
