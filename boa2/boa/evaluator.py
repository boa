"""Lisp evaluator

Recursion based evaluator, such as this, is doomed to overflow
the Python stack. CPS is one way to solve this issue.
TODO: investigate more about this.
"""

from boa.error import *
from boa.primitives import *
from boa.environment import Environment

def eval(exp, env):
    # boa.macro imports evaluator.py and so we delay the import
    # of boa.macro here
    from boa.macro import *
    
    if is_self_evaluating(exp):
        return exp
    if is_variable(exp):
        return env.lookup(symbol_to_string(exp))
        # return lookup_var_or_keyword(env, symbol_to_string(exp))
    if is_quoted(exp):
        return text_of_quotation(exp)
    if is_assignment(exp):
        return eval_assignment(exp, env)
    if is_definition(exp):
        return eval_definition(exp, env)
    if is_syntax_definition(exp):
        return eval_syntax_definition(exp, env)
    if is_if(exp):
        return eval_if(exp, env)
    if is_lambda(exp):
        return make_procedure(lambda_parameters(exp),
                              lambda_body(exp),
                              env)
    if is_begin(exp):
        return eval_sequence(begin_actions(exp), env)
    if is_cond(exp):
        return eval(cond_to_if(exp), env)
    if is_application(exp):
        op = operator(exp)
        if symbol_p(op):
            op = keyword(symbol_to_string(op))
            if op == keyword("syntax-case"):
                a = operands(exp)
                return eval(eval_syntax_case(eval(car(a), env), # stx
                                        cadr(a),                # (literals ...)
                                        cddr(a),                # ((p1, c1) ...)
                                        ), env)
            elif op == keyword("syntax"):
                pass
            elif op in env:
                print 'booooo', op
                # this is a macro
                transformer = env.lookup(op)
                if is_compound_procedure(transformer):
                    args = {symbol_to_string(
                            car(procedure_parameters(transformer))):
                            operands(exp)}
                    return eval_sequence(
                        procedure_body(transformer),
                        Environment(procedure_environment(transformer), args))
                else:
                    error("SYNTAX must be a procedure (syntax-case): %s", op)
        # this could be normal procedure application
        op = eval(operator(exp), env)
        return apply(op, list_of_values(operands(exp), env))
    # else
    error("Unknown expression type -- EVAL: %s of type %s", exp, type(exp))

def apply(procedure, arguments):
    if is_primitive_procedure(procedure):
        return apply_primitive_procedure(procedure, arguments)
    if is_compound_procedure(procedure):
        args = {}
        for s, v in zip(pylist(procedure_parameters(procedure)),
                        pylist(arguments)):
            args[symbol_to_string(s)] = v
        return eval_sequence(
            procedure_body(procedure),
            Environment(procedure_environment(procedure), args))
    # else
    error("Unknown procedure type -- APPLY: %s", procedure)

def list_of_values(exps, env):
    if no_operands(exps):
        return None
    else:
        return cons(eval(first_operand(exps), env),
                    list_of_values(rest_operands(exps), env))

def eval_if(exp, env):
    if is_true(eval(if_predicate(exp), env)):
        return eval(if_consequent(exp), env)
    else:
        return eval(if_alternative(exp), env)

def eval_sequence(exps, env):
    if is_last_exp(exps):
        return eval(first_exp(exps), env)
    else:
        eval(first_exp(exps), env)
        return eval_sequence(rest_exps(exps), env)

def eval_assignment(exp, env):
    env.set(symbol_to_string(assignment_variable(exp)),
            eval(assignment_value(exp), env))
    return symbol("ok")

def eval_definition(exp, env):
    env.define(symbol_to_string(definition_variable(exp)),
               eval(definition_value(exp), env))
    return symbol("ok")

def is_self_evaluating(exp):
    return number_p(exp) or string_p(exp)

def is_variable(exp):
    return symbol_p(exp)

def is_quoted(exp):
    return is_tagged_list(exp, symbol("quote"))

def text_of_quotation(exp):
    return cadr(exp)

def is_tagged_list(exp, tag):
    return pair_p(exp) and eq_p(car(exp), tag)

def is_assignment(exp):
    return is_tagged_list(exp, symbol("set!"))

def assignment_variable(exp):
    return cadr(exp)

def assignment_value(exp):
    return caddr(exp)

def is_definition(exp):
    return is_tagged_list(exp, symbol("define"))

def definition_variable(exp):
    if symbol_p(cadr(exp)):
        return cadr(exp)
    else:
        return caadr(exp)

def definition_value(exp):
    if symbol_p(cadr(exp)):
        return caddr(exp)
    else:
        return make_lambda(cdadr(exp),
                           cddr(exp))

def is_lambda(exp):
    return is_tagged_list(exp, symbol("lambda"))

def lambda_parameters(exp):
    return cadr(exp)

def lambda_body(exp):
    return cddr(exp)

def make_lambda(parameters, body):
    return cons(symbol("lambda"),
                cons(parameters, body))

def is_if(exp):
    return is_tagged_list(exp, symbol("if"))

def if_predicate(exp):
    return cadr(exp)

def if_consequent(exp):
    return caddr(exp)

def if_alternative(exp):
    if not null_p(cdddr(exp)):
        return cadddr(exp)
    else:
        return symbol("FALSE")

def make_if(predicate, consequent, alternative):
    return list(symbol("IF"),
                 predicate, consequent, alternative)

def is_begin(exp):
    return is_tagged_list(exp, symbol("begin"))

def begin_actions(exp):
    return cdr(exp)

def is_last_exp(seq):
    return null_p(cdr(seq))

def first_exp(seq):
    return car(seq)

def rest_exps(seq):
    return cdr(seq)

def sequence_to_exp(seq):
    if null_p(seq):
        return seq
    elif is_last_exp(seq):
        return first_exp(seq)
    else:
        return make_begin(seq)

def make_begin(seq):
    return cons(symbol("begin"),
                seq)

def is_application(exp):
    return pair_p(exp)

def operator(exp):
    return car(exp)

def operands(exp):
    return cdr(exp)

def no_operands(ops):
    return null_p(ops)

def first_operand(ops):
    return car(ops)

def rest_operands(ops):
    return cdr(ops)

# The following are `derived' expressions (apt to be macros)

def is_cond(exp):
    return is_tagged_list(exp, symbol("cond"))

def cond_clauses(exp):
    return cdr(exp)

def is_cond_else_clause(clause):
    return eq_p(cond_predicate(clause),
                symbol("ELSE"))

def cond_predicate(clause):
    return car(clause)

def cond_actions(clause):
    return cdr(clause)

def cond_to_if(exp):
    return expand_clauses(cond_clauses(exp))

def expand_clauses(clauses):
    if null_p(clauses):
        return symbol("FALSE")
    else:
        first = car(clauses)
        rest  = cdr(clauses)
        if is_cond_else_clause(first):
            if null_p(rest):
                sequence_to_exp(cond_actions(first))
            else:
                error("ELSE clause isn't last -- COND->IF: %s'" % clauses)
        else:
            return make_if(cond_predicate(first),
                           sequence_to_exp(cond_actions(first)),
                           expand_clauses(rest))

def make_procedure(parameters, body, env):
    return list(symbol("procedure"),
                parameters, body, env)

def is_compound_procedure(p):
    return is_tagged_list(p, symbol("procedure"))

def procedure_parameters(p):
    return cadr(p)

def procedure_body(p):
    return caddr(p)

def procedure_environment(p):
    return cadddr(p)

def is_primitive_procedure(proc):
    return callable(proc)

def apply_primitive_procedure(proc, args):
    return proc(*pylist(args))

def is_true(e):
    return e is True

def is_false(e):
    return e is False
