"""Lisp syntax parser

This module is deliberately kept simple (and inefficient). The parse
tree is just a cons list (code is data), so that implementing things
like `quote` and `define-syntax` will be straightforward.
"""

import re

from boa.error import *
from boa.primitives import symbol, list as cons_list
from boa.evaluator import sequence_to_exp

c = re.compile
PATTERNS = [
    ('whitespace',     c(r'(\s+)')),
    ('comment',        c(r'(;[^\n]*)')),
    ('(',              c(r'(\()')),
    (')',              c(r'(\))')),
    ('number',         c(r'''( [+\-]?    ## optional sign,
                               (?:       ## followed by some
                                         ## decimals
                                    \d+\.\d+
                                  | \d+\.
                                  | \.\d+
                                  | \d+
                                )
                              )
                                ''',
                         re.VERBOSE)),
    ('symbol',         c(r'''([a-zA-Z\+\=\?\!\@\#\$\%\^\&\*\-\_\/\.\>\<]
                              [\w\+\=\?\!\@\#\$\%\^\&\*\-\_\/\.\>\<]*)''',
                         re.VERBOSE)),
    ('string',         c(r'''
                           "
                           (([^\"] | \\")*)
                           "
                           ''',
                         re.VERBOSE)),
    ("'",              c(r'(\')')),
    ('`',              c(r'(`)')),
    (',',              c(r'(,)')),
    ]

def tokenize(s):
    tokens = []
    while s:
        m = None
        for type, regex in PATTERNS:
            m = regex.match(s)
            if m:
                token = m.group(1)
                tokens.append((type, token))
                s = s[m.span()[1]:]
                break
        if not m:
            error("TOKENIZE error from: %s..." % s[:20])
    return tokens

def filter_executable_tokens(tokens):
    return filter(
        lambda x: x[0] not in ('whitespace', 'comment'),
        tokens)

def parse(text):
    tokens = filter_executable_tokens(tokenize(text))
    sexps, n = [], 0
    while n < len(tokens):
        sexp, n = parse_sexp(tokens, n)
        sexps.append(sexp)
    return sequence_to_exp(cons_list(*sexps))

def parse_sexp(tokens, n):
    if tokens[n][0] is 'string':
        return tokens[n][1], n+1
    if tokens[n][0] is 'number':
        return int(tokens[n][1]), n+1 # ??
    if tokens[n][0] is 'symbol':
        return symbol(tokens[n][1]), n+1
    if tokens[n][0] is "'":
        e, n = parse_sexp(tokens, n+1)
        return cons_list(symbol("quote"), e), n
    if tokens[n][0] == '(':
        sexps, n = [], n+1
        while tokens[n][0] != ')':
            e, n = parse_sexp(tokens, n)
            sexps.append(e)
        return cons_list(*sexps), n+1
    error("PARSE error -- Invalid/unsupported token: %s" % tokens[n][0])
