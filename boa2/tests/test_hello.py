
from boa.runtime import Interpreter

def test_hello_world():
    i = Interpreter()
    v = i.push(r"""
(begin
  (define s "Hello World")
  (display s))
""")
    
