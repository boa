import os
from nose import with_setup

from boa.primitives import *
from boa.runtime import Interpreter

i = None
def setup():
    global i
    i = Interpreter()

@with_setup(setup)
def test_assignment():
    i.push('(define a "foo")')
    v = i.push("(cons a a)")
    a = cons('foo', 'foo')
    assert eq_p(v, a), (v, a)
    
    i.push('(define b 2)')
    v = i.push('b')
    assert eq_p(v, 2), type(v)

@with_setup(setup)
def test_if():
    v = i.push(r"""
(begin
  (define a true)
  (if a 3 4))
""")
    assert eq_p(v, 3), v

@with_setup(setup)
def test_procedures():
    v = i.push(r"""
(begin
  (define (foo x y)
    (+ x
       (+ y x)))
  (foo 4 5))
""")
    assert eq_p(v, 13)

    v = i.push(r"""
(begin
  (define (bar x)
    (+ x
       ((lambda (a) (+ a a)) x)))
  (bar 2))
""")
    assert eq_p(v, 6), v

@with_setup(setup)
def test_closure():
    v = i.push(r"""
(begin
  (define (counter n)
    (define (inc)
      (set! n (+ n 1))
      n)
    inc)
  (define a1 (counter 3))
  (define a2 (counter 5))
  (a1)
  (a2)
  (a1)
  (a1)
  (a2)
  (cons (a1) (a2)))
""")
    assert eq_p(v, cons(7, 8))

@with_setup(setup)
def XX_import():
    v = i.push(r"""
  (define os (import "os"))
  ((. os "getlogin"))
""")
    assert eq_p(v, os.getlogin()), v

def test_import_hook():
    import foo
    assert foo.bar == 3

@with_setup(setup)
def test_syntax_case():
    v = i.push(r"""
 (define-syntax swap! 
   (lambda (stx) 
     (syntax-case stx () 
       ((_ a b) 
        (syntax 
         (let ((value a)) 
           (set! a b) 
           (set! b value))))))) 
 
 (define a 5)
 (define b 1)
 (- b a)
 (swap! a b)
 (- b a)
""")
    assert eq_p(v, 5), v

@with_setup(setup)
def XX_macro():
    v = i.push(r"""
(define-syntax when
  (syntax-rules ()
    ((when c e) (if c e))))

(define a true)
(when a (+ 3 4))
""")
    assert eq_p(v, 7), v
