from boa.primitives import *

def test_pair():
    x = cons(1, 2)
    assert car(x) is 1
    assert cdr(x) is 2
    set_car(x, 4)
    assert car(x) is 4
    set_cdr(x, 5)
    assert cdr(x) is 5

def test_set_pair():
    x = cons(1, cons(2, 3))
    set_car(cdr(x), 100)
    assert car(cdr(x)) is 100

