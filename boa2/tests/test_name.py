
from boa import name

def test_name_mangling():
    names = [
        ("foo_bar_dish", "foo-bar-dish"),
        ("set_foo_bar", "set-foo-bar!"),
        ("null_p", "null?"),
        ("foo_to_bar_1", "foo->bar-1")]
    for p, l in names:
        l2 = name.py2lisp(p)
        p2 = name.lisp2py(l)
        assert l2 == l, (l2, l)
        assert p2 == p, (p2, p)
